# Semaphore
A [semaphore](https://en.wikipedia.org/wiki/Semaphore_(programming)) that enables controlled access to a limited ressource in a multi-threaded setting; sometimes called "counting" semaphore, because the controlled ressource has a certain magnitude.

May be used to simply limit concurrency of execution, or maybe amount of (GPU) memory that all threads are collectively allowed to use. A counting semaphore is available in the stl from C++20 onwards. Since C++11 it can be mimicked by using other synchronisation primitives.

Construct a semaphore by calling the factory method while specifying base type and ressource limit. Then acquire allowances for some amount of units of that ressource. Think of the allowance as a ticket: you can safely use the ressource as long as you stay in posession of the ticket. You must cease use of the ressource (free it, where applicacle) before discarding the ticket. The moment the allowance object is destructed, you're discarding the ticket.