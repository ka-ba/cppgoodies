#include "Semaphore.h"
#include <boost/test/unit_test.hpp>
#include <algorithm>
#include <execution>
#include <mutex>
#include <thread>
#include <vector>

constexpr uint8_t avail{5};

BOOST_AUTO_TEST_SUITE( semaphoreMultiThread );

BOOST_AUTO_TEST_CASE( stayUnderLimit ) {
    using namespace std::chrono_literals;
    auto sem = Semaphore<uint8_t>::construct( avail );
    std::atomic_size_t concurrency{0};
    std::vector<char> work( 15 );
    BOOST_REQUIRE( work.size() == 15 ); // check vector init
    std::mutex mutex{};
    std::for_each( std::execution::par, work.cbegin(), work.cend(),
                   [&sem,&concurrency,&mutex](char c){
        auto a = sem->acquire( 1 );
        ++concurrency;
        std::this_thread::sleep_for( 200ms );
        {   std::lock_guard<std::mutex> guard{mutex};
            BOOST_WARN_EQUAL( concurrency, avail ); // may warn few times near end
            BOOST_CHECK_LE( concurrency, avail );
        }
        --concurrency;
    });
    BOOST_CHECK_EQUAL( sem->currentAvail(), avail );
    BOOST_CHECK_EQUAL( concurrency, 0 );
}

BOOST_AUTO_TEST_SUITE_END();
