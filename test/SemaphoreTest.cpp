#include "Semaphore.h"
#include <boost/test/unit_test.hpp>

constexpr uint8_t avail=5;

BOOST_AUTO_TEST_SUITE( semaphoreSingleThread );

BOOST_AUTO_TEST_CASE( basic ) {
    auto sem = Semaphore<uint8_t>::construct( avail );
    BOOST_REQUIRE( sem->currentAvail() == avail );
    {
        auto a0 = sem->acquire( 1 );
        BOOST_CHECK( sem->currentAvail() == avail-1 );
        {
            auto a1 = sem->acquire( 4 );
            BOOST_CHECK( sem->currentAvail() == avail-1-4 );
        }
        BOOST_CHECK( sem->currentAvail() == avail-1 );
    }
    BOOST_CHECK( sem->currentAvail() == avail );
}

BOOST_AUTO_TEST_CASE( exceedAvail ) {
    auto sem = Semaphore<uint8_t>::construct( avail );
    BOOST_CHECK_THROW( sem->acquire(avail+1), std::invalid_argument );
}

BOOST_AUTO_TEST_SUITE_END();
