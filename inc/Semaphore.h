/* Copyright (C) 2020-2021 kaba
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/.
 **/
#pragma once
#include <atomic>
#include <cassert>
#include <condition_variable>
template<class T> class Allowance;

template<class T>
class Semaphore
{
public:
    static std::shared_ptr<Semaphore<T>> construct( T const supply );
    Semaphore(Semaphore const&) = delete;
    Semaphore(Semaphore&&) = delete;
    Semaphore &operator=(Semaphore const&) = delete;
    Semaphore &operator=(Semaphore&&) = delete;
    [[nodiscard]]Allowance<T> acquire( T const demand ); // throws
    T currentAvail() const { return current_; }
private:
    friend class Allowance<T>;
    explicit Semaphore( T const supply ) : supply_(supply), current_(supply) {}
    void release( T const amount ) noexcept;
    T const supply_;
    std::atomic<T> current_;
    std::weak_ptr<Semaphore<T>> self_{};  // MUST be set by factory method!
    std::mutex mutex_{};
    std::condition_variable cv_{};
};

template<class T>
std::shared_ptr<Semaphore<T>> Semaphore<T>::construct( T const supply ) {
    auto inst = std::shared_ptr<Semaphore<T>>( new Semaphore{supply} ); // can't use make_shared due to privacy concerns
    inst->self_ = inst;
    return inst;
}

template<class T>
Allowance<T> Semaphore<T>::acquire( T const demand ) {
    if( demand > supply_ )
        throw std::invalid_argument{ "demand exceeds supply" };
    auto me = std::shared_ptr{ self_ }; // also able to throw
    {   std::unique_lock<std::mutex> ulock{mutex_};
        cv_.wait( ulock, [this,&demand]{return current_>=demand;} );
        T cur = current_;
        do
            assert( cur >= demand ); // cur cannot shrink, because subtractions protected by mutex
        while( ! current_.compare_exchange_weak(cur,cur-demand) );
    }
    cv_.notify_all();
    return Allowance{ me, demand }; //potentially dangerous, but accepted here
}

template<class T>
void Semaphore<T>::release( T const amount ) noexcept {
    T cur = current_;
    while( ! current_.compare_exchange_weak(cur,cur+amount) ); // current may raise or fall concurrently
    cv_.notify_all();
}

template<class T>
class Allowance
{
public:
    ~Allowance() { if(semaphore_) semaphore_->release(acquired_); }
    Allowance() = delete;
    Allowance(Allowance<T> const&) = delete;
    Allowance( Allowance<T> &&other ) {
        *this = std::move( other );
    }
    Allowance &operator=(Allowance<T> const&) = delete;
    Allowance &operator=( Allowance<T> &&other ) {
        if( &other == this )
            return *this;
        semaphore_ = std::move( other.semaphore_ );
        acquired_ = std::move( other.acquired_ );
        other.acquired_ = 0; // safety net
        assert( ! other.semaphore_ ); // double-check
        return *this;
    }
private:
    friend class Semaphore<T>;
    Allowance( std::shared_ptr<Semaphore<T>> const &sem, T const acquired ) noexcept : semaphore_(sem), acquired_(acquired) {}
    std::shared_ptr<Semaphore<T>> semaphore_;
    T acquired_;
};
